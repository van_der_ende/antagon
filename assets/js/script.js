// @codekit-prepend "plugins"

var View = Backbone.View,
    ActOne, ActOneClone, App, app;

function touchView() {
  var windowWidth = $(window).width();
  return windowWidth <= 980 || $('html').hasClass('touch');
}

/*
ActOne = View.extend({
  el: '#act-1',
  initialize: function() {
    var _this = this;

    this.$stage = this.$('.stage');
    this.$video = this.$('.video');
    this.$shadow = this.$('.shadow');
    this.video = this.$('video')[0];
    this.canvas = this.$('canvas')[0];
    this.context = this.canvas.getContext('2d');

    this.ticksSinceSwitch = 0;
    this.switchOnTick = 990;

    this.position = {
      x: this.$video.position().left,
      y: this.$video.position().top
    };

    this.dimensions = {
      w: this.$video.outerWidth(),
      h: this.$video.outerHeight(),
      wW: this.$stage.outerWidth(),
      wH: this.$stage.outerHeight(),
      sW: 800,
      sH: 800,
      maxStep: 4,
      maxW: this.$stage.outerWidth(),
      minW: 500,
      direction: 1
    };

    $(window).on('mousemove', _.bind(this._onMousemove, this));

    this.video.play();
  },
  move: function(x, y) {
    this.position.x = x;
    this.position.y = y;
    this.draw();

    this.$video.css({ top: y, left: x });
    this.$shadow.css({ top: y, left: x });
  },
  resize: function(w, sW, sH) {
    this.dimensions.w = w;
    this.dimensions.h = Math.round(9 / 16 * w);
    this.dimensions.sW = sW;
    this.dimensions.sH = sH;
    this.draw();

    this.$video.css({ width: this.dimensions.w, height: this.dimensions.h, margin: '-' + (this.dimensions.h/2) + 'px -' + (this.dimensions.w/2) + 'px' });
    this.$shadow.css({ width: this.dimensions.sW, height: this.dimensions.sH, margin: '-' + (this.dimensions.sH/2) + 'px -' + (this.dimensions.sW/2) + 'px' });
  },
  activate: function() {
    this.video.volume = 1;
    //this.video.playbackRate = 1.0;
    //this.dimensions.maxStep = 5;
  },
  deactivate: function() {
    this.video.volume = 0;
    //this.video.playbackRate = 0.1;
    //this.dimensions.maxStep = 1;
  },
  draw: function() {
    this.dimensions.wW = this.$stage.outerWidth();
    this.dimensions.wH = this.$stage.outerHeight();
    this.dimensions.maxW = this.$stage.outerWidth();
    this.canvas.width = this.dimensions.w;
    this.canvas.height = this.dimensions.h;
    this.context.drawImage(this.video, this.position.x - (this.dimensions.w/2), this.position.y - (this.dimensions.h/2), this.dimensions.w, this.dimensions.h);
  },
  _onMousemove: function(evt) {
    this.move(evt.pageX, evt.pageY);
  },
  _resizeTick: function() {
    var step = (Math.floor(Math.random()*this.dimensions.maxStep)) * this.dimensions.direction,
        swStep = (Math.floor(Math.random()*this.dimensions.maxStep)) * this.dimensions.direction *-0.75,
        shStep = (Math.floor(Math.random()*this.dimensions.maxStep)) * this.dimensions.direction,
        w = this.dimensions.w,
        sW = this.dimensions.sW,
        sH = this.dimensions.sH;
    if ( (step + w) > this.dimensions.maxW || (step + w) < this.dimensions.minW ) {
      this.ticksSinceSwitch = 0;
      this.switchOnTick = 990;//Math.floor(Math.random()*200) + 20;
      step = step * -1;
      swStep = swStep * -1;
      shStep = shStep * -1;
      this.dimensions.direction = this.dimensions.direction * -1;
    } else if ( this.ticksSinceSwitch >= this.switchOnTick ) {
      this.ticksSinceSwitch = 0;
      this.switchOnTick = 990;//Math.floor(Math.random()*200) + 20;
      step = step * -1;
      swStep = swStep * -1;
      shStep = shStep * -1;
      this.dimensions.direction = this.dimensions.direction * -1;
    } else {
      this.ticksSinceSwitch++;
    }
    w = w + step;
    sW = sW + swStep;
    sH = sH + shStep;
    this.resize(w, sW, sH);
  }
});
*/

ActOne = View.extend({
  el: '#act-1',
  initialize: function() {
    this.$stage = this.$('.stage');
    this.$shadow = this.$('.shadow');
    this.$video = this.$('.video');
    this.video = this.$('video')[0];

    this.dimensions = {
      windowWidth: $(window).width(),
      windowHeight: $(window).height(),
      originalWidth: this.$video.outerWidth(),
      originalHeight: this.$video.outerHeight(),
      originalRatio: this.$video.outerWidth() / this.$video.outerHeight(),
      stageWidth: 0,
      stageHeight: 0,
      stageRatio: 0,
      videoMaxWidth: 0,
      videoMaxHeight: 0,
      videoMinWidth: 0,
      videoMinHeight: 0,
      videoWidth: 0,
      videoHeight: 0,
      shadowMaxWidth: 0,
      shadowMaxHeight: 0,
      shadowMinWidth: 20,
      shadowMinHeight: 20,
      shadowWidth: 0,
      shadowHeight: 0
    };

    this.position = {
      x: 0,
      y: 0
    };

    this.progress = {
      videoSize: {
        direction: 1,
        ratio: 0,
        speed: 0.3
      },
      shadowWidth: {
        direction: 1,
        ratio: 0,
        speed: 0.2
      },
      shadowHeight: {
        direction: -1,
        ratio: 100,
        speed: 0.5
      },
      increment: function(prop, isActive) {
        var speed = isActive ? prop.speed : prop.speed / 8,
            direction = prop.direction,
            ratio = prop.ratio + prop.direction * speed;

        if (this.isDeactivated) ratio
        
        if (ratio > 100) {
          ratio = 100;
          direction = -1;
        } else if (ratio < 0) {
          ratio = 0;
          direction = 1;
        }

        prop.ratio = ratio;
        prop.direction = direction;
        return;
      }
    }

    this.video.play();

    $(window).on('mousemove', _.bind(this._onMousemove, this)).trigger('mousemove');
    $(window).on('resize', _.bind(this._onWindowResize, this)).trigger('resize');
  },
  activate: function() {
    this.isActive = true;
    this.video.volume = 1;
    this.resizeCounter = 0;
    this.isResizing();
  },
  deactivate: function() {
    this.isActive = false;
    this.video.volume = 0;
    this.resizeCounter = 0;
    this.isResizing();
  },
  animationTick: function() {
    this.progress.increment(this.progress.videoSize, this.isActive);
    this.progress.increment(this.progress.shadowWidth, this.isActive);
    this.progress.increment(this.progress.shadowHeight, this.isActive);

    var videoNewWidth = (this.dimensions.videoMaxWidth - this.dimensions.videoMinWidth) * this.progress.videoSize.ratio / 100 + this.dimensions.videoMinWidth,
        videoNewHeight = this.dimensions.originalHeight / this.dimensions.originalWidth * videoNewWidth,
        shadowNewWidth = (this.dimensions.shadowMaxWidth - this.dimensions.shadowMinWidth) * this.progress.shadowWidth.ratio / 100 + this.dimensions.shadowMinWidth,
        shadowNewHeight = (this.dimensions.shadowMaxHeight - this.dimensions.shadowMinHeight) * this.progress.shadowHeight.ratio / 100 + this.dimensions.shadowMinHeight;

    this.dimensions.videoWidth = videoNewWidth;
    this.dimensions.videoHeight = videoNewHeight;
    this.dimensions.shadowWidth = shadowNewWidth;
    this.dimensions.shadowHeight = shadowNewHeight;

    this.$video.css({
      width: this.dimensions.videoWidth,
      margin: '-' + (this.dimensions.videoHeight / 2) + 'px -' + (this.dimensions.videoWidth / 2) + 'px',
      left: this.position.x * this.dimensions.stageWidth,
      top: this.position.y * this.dimensions.stageHeight
    });

    this.$shadow.css({
      width: this.dimensions.shadowWidth,
      height: this.dimensions.shadowHeight,
      margin: '-' + (this.dimensions.shadowHeight / 2) + 'px -' + (this.dimensions.shadowWidth / 2) + 'px',
      left: this.position.x * this.dimensions.stageWidth,
      top: this.position.y * this.dimensions.stageHeight
    });
  },
  isResizing: function() {
    this.isResizingTimeout = setTimeout(_.bind(this.isResizingTick, this), 10);
  },
  isResizingTick: function() {
    if ( this.resizeCounter >= 300 ) {
      this.resizeCounter = 0;
      clearTimeout(this.isResizingTimeout);
    } else {
      this.resizeCounter += 10;
      this.isResizingTimeout = setTimeout(_.bind(this.isResizingTick, this), 10);
      this._onWindowResize();
    }
  },
  _onMousemove: function(evt) {
    this.position.x = evt.pageX / this.dimensions.windowWidth;
    this.position.y = evt.pageY / this.dimensions.windowHeight;
  },
  _onWindowResize: function() {
    this.dimensions.windowWidth = $(window).width();
    this.dimensions.windowHeight = $(window).height();
    this.dimensions.stageWidth = this.$stage.outerWidth();
    this.dimensions.stageHeight = this.$stage.outerHeight();
    this.dimensions.stageRatio = this.dimensions.stageWidth / this.dimensions.stageHeight;

    if ( this.dimensions.stageRatio > this.dimensions.videoRatio ) {
      this.dimensions.videoMaxHeight = this.dimensions.stageHeight;
      this.dimensions.videoMaxWidth = this.dimensions.originalWidth / this.dimensions.originalHeight * this.dimensions.videoMaxHeight;
    } else {
      this.dimensions.videoMaxWidth = this.dimensions.stageWidth;
      this.dimensions.videoMaxHeight = this.dimensions.originalHeight / this.dimensions.originalWidth * this.dimensions.videoMaxWidth;
    }

    this.dimensions.videoMinWidth = this.dimensions.videoMaxWidth / 1.5;
    this.dimensions.videoMinHeight = this.dimensions.videoMaxHeight / 1.5;

    this.dimensions.shadowMaxWidth = this.dimensions.stageWidth * 1.75;
    this.dimensions.shadowMaxHeight = this.dimensions.stageHeight * 1.75;
  }
});

ActTwo = View.extend({
  el: '#act-2',
  initialize: function() {
    this.$stage = this.$('.stage');
    this.$video = this.$('.video');
    this.video = this.$('video')[0];

    this.canvas = this.$('canvas')[0];
    this.ctx = this.canvas.getContext('2d');
    this.ctx.fillColor = '#ff0000';

    this.tailDelay = 100;
    this.tail = [];

    this.dimensions = {
      windowWidth: $(window).width(),
      windowHeight: $(window).height(),
      originalWidth: this.$video.outerWidth(),
      originalHeight: this.$video.outerHeight(),
      originalRatio: this.$video.outerWidth() / this.$video.outerHeight(),
      stageWidth: 0,
      stageHeight: 0,
      stageRatio: 0,
      videoWidth: 0,
      videoHeight: 0
    };

    this.position = {
      x: 0,
      y: 0,
      maxX: 0,
      minX: 0,
      maxY: 0,
      minY: 0
    };

    this.progress = {
      videoX: {
        direction: 1,
        ratio: 30,
        speed: 0.2
      },
      videoY: {
        direction: 1,
        ratio: 57,
        speed: 0.1
      },
      increment: function(prop, isActive) {
        var speed = isActive ? prop.speed : prop.speed / 2,
            direction = prop.direction,
            ratio = prop.ratio + prop.direction * speed;

        if (this.isDeactivated) ratio
        
        if (ratio > 100) {
          ratio = 100;
          direction = -1;
        } else if (ratio < 0) {
          ratio = 0;
          direction = 1;
        }

        prop.ratio = ratio;
        prop.direction = direction;
        return;
      }
    }

    this.video.play();

    $(window).on('resize', _.bind(this._onWindowResize, this)).trigger('resize');
  },
  activate: function() {
    this.isActive = true;
    this.video.volume = 1;
    this.resizeCounter = 0;
    this.isResizing();
  },
  deactivate: function() {
    this.isActive = false;
    this.video.volume = 0;
    this.resizeCounter = 0;
    this.isResizing();
  },
  animationTick: function() {
    this.progress.increment(this.progress.videoX, this.isActive);
    this.progress.increment(this.progress.videoY, this.isActive);

    var videoNewX = (this.position.maxX - this.position.minX) * this.progress.videoX.ratio / 100 + this.position.minX,
        videoNewY = (this.position.maxY - this.position.minY) * this.progress.videoY.ratio / 100 + this.position.minY,
        piece = null;

    if (this.tail.length && this.video.currentTime < 38.5) {
      this.tail = [];
      this.tailDelay = 100;
    } else if ( this.video.currentTime >= 38.5 && this.tailDelay === 100 ) {
      piece = {
        x: videoNewX / this.dimensions.stageWidth * 100,
        y: videoNewY / this.dimensions.stageHeight * 100,
        size: 100
      };
      this.tailDelay = 0;
    } else if ( this.video.currentTime >= 38.5 ) {
      this.tailDelay++;      
    }

    this.$video.css({
      width: this.dimensions.videoWidth,
      margin: '-' + (this.dimensions.videoHeight / 2) + 'px -' + (this.dimensions.videoWidth / 2) + 'px',
      left: videoNewX,
      top: videoNewY
    });
    
    this.canvas.width = this.dimensions.stageWidth;
    this.canvas.height = this.dimensions.stageHeight;
    this.drawTail(piece);
  },
  drawTail: function(newPiece) {
    var i, piece, x, y, width, height, r, g, b;

    if (newPiece) this.tail.push(newPiece);

    for (i = 0; i < this.tail.length; i++) {
      piece = this.tail[i];

      width = piece.size / 100 * this.dimensions.videoWidth;
      height = piece.size / 100 * this.dimensions.videoHeight;
      x = (piece.x / 100 * this.dimensions.stageWidth) - (width / 2);
      y = (piece.y / 100 * this.dimensions.stageHeight) - (height / 2);

      this.ctx.drawImage(this.video, x, y, width, height);
    }
  },
  isResizing: function() {
    this.isResizingTimeout = setTimeout(_.bind(this.isResizingTick, this), 10);
  },
  isResizingTick: function() {
    if ( this.resizeCounter >= 300 ) {
      this.resizeCounter = 0;
      clearTimeout(this.isResizingTimeout);
    } else {
      this.resizeCounter += 10;
      this.isResizingTimeout = setTimeout(_.bind(this.isResizingTick, this), 10);
      this._onWindowResize();
    }
  },
  _onWindowResize: function() {
    this.dimensions.windowWidth = $(window).width();
    this.dimensions.windowHeight = $(window).height();
    this.dimensions.stageWidth = this.$stage.outerWidth();
    this.dimensions.stageHeight = this.$stage.outerHeight();
    this.dimensions.stageRatio = this.dimensions.stageWidth / this.dimensions.stageHeight;

    if ( this.dimensions.stageRatio > this.dimensions.videoRatio ) {
      this.dimensions.videoHeight = this.dimensions.stageHeight * 0.75;
      this.dimensions.videoWidth = this.dimensions.originalWidth / this.dimensions.originalHeight * this.dimensions.videoHeight;
    } else {
      this.dimensions.videoWidth = this.dimensions.stageWidth * 0.75;
      this.dimensions.videoHeight = this.dimensions.originalHeight / this.dimensions.originalWidth * this.dimensions.videoWidth;
    }

    this.position.minX = this.dimensions.videoWidth / 2;
    this.position.maxX = this.dimensions.stageWidth - this.dimensions.videoWidth + (this.dimensions.videoWidth / 2);
    this.position.minY = this.dimensions.videoHeight / 2;
    this.position.maxY = this.dimensions.stageHeight - this.dimensions.videoHeight + (this.dimensions.videoHeight / 2);
  }
});

ChatWindow = View.extend({
  events: {
    'mouseenter': 'focus',
    'mouseleave': 'blur',
    'click .title a': 'toggleOpen'
  },
  initialize: function() {
    setTimeout(_.bind(this.render, this), this.options.delay);
    setTimeout(_.bind(function() { this.$el.removeClass('flashing'); }, this), this.options.delay + 5000);
    this.jsp = this.$('.conversation').jScrollPane({autoReinitialise: true, autoReinitialiseDelay: 100, hideFocus: true}).data().jsp;
    this.isNew = true;
    $(window).on('resize', _.bind(this.onWindowResize, this)).resize();
  },
  render: function() {
    if ( this.isNew ) {
      this.$el.removeClass('off');
      this.$el.addClass('new flashing');
    }
  },
  focus: function() {
    if ( this.$el.siblings('.open').length < 1 ) {
      this.$el.removeClass('blur').addClass('focus');
      this.$el.siblings().removeClass('focus').addClass('blur');
    }
  },
  blur: function() {
    if ( this.$el.siblings('.open').length < 1 && ! this.$el.hasClass('open') ) {
      this.$el.removeClass('blur').removeClass('focus');
      this.$el.siblings().removeClass('blur').removeClass('focus');
    }
  },
  toggleOpen: function() {
    if ( this.isNew ) {
      this.$el.removeClass('off');
      this.$el.removeClass('new');
      this.$el.removeClass('flashing');
      this.isNew = false;
    }
    if ( this.$el.hasClass('open') ) {
      this.$el.removeClass('blur').removeClass('focus').removeClass('open');
      this.$el.siblings().removeClass('focus').removeClass('blur');
    } else {
      this.$el.removeClass('blur').addClass('focus').addClass('open');
      this.$el.siblings().removeClass('focus').removeClass('open').addClass('blur');
      if ( touchView() ) $(window).scrollTop(this.$el.offset().top);
    }
    return false;
  },
  onWindowResize: function() {
    var windowWidth = $(window).width();
    if ( ! touchView() ) {
      if ( ! this.jsp ) this.jsp = this.$('.conversation').jScrollPane({autoReinitialise: true, autoReinitialiseDelay: 100, hideFocus: true}).data().jsp;
    } else {
      if ( this.jsp && this.jsp.destroy ) { this.jsp.destroy(); this.jsp = null; }
    }
  }
});

App = View.extend({
  el: 'body',
  events: {
    'click a:not([data-external])': 'linkHandler'
  },
  initialize: function() {
    this.router = new Router();
    this.acts = {
      'act-2': new ActTwo(),
      'act-1': new ActOne()
    };

    this.animate();

    var delay = 0;
    this.$chatWindows = this.$('.chat-window');
    this.$chatWindows.each(function(i) {
      delay += Math.floor(Math.random()*3000) + 5000;
      var chat = new ChatWindow({ el: $(this), delay: delay });
    });

    this.jsp1 = this.$('.primary').jScrollPane({autoReinitialise: true, hideFocus: true}).data().jsp;
    this.jsp2 = this.$('.secondary').jScrollPane({autoReinitialise: true, hideFocus: true}).data().jsp;

    $(window).on('resize', _.bind(this.onWindowResize, this)).resize();
  },
  render: function(state) {
    this.$el.attr('class', 'state-' + state);
  },
  animate: function() {
    requestAnimationFrame( _.bind(this.animate, this) );
    this.acts['act-2'].animationTick();
    this.acts['act-1'].animationTick();
  },
  linkHandler: function(evt) {
    var href = $(evt.currentTarget).attr('href');
    app.router.navigate(href, true);
    return false;
  },
  onWindowResize: function() {
    if ( ! touchView() ) {
      if ( ! this.jsp1 ) this.jsp1 = this.$('.primary').jScrollPane({autoReinitialise: true, hideFocus: true}).data().jsp;
      if ( ! this.jsp2 ) this.jsp2 = this.$('.secondary').jScrollPane({autoReinitialise: true, hideFocus: true}).data().jsp;
    } else {
      if ( this.jsp1 && this.jsp1.destroy ) { this.jsp1.destroy(); this.jsp1 = null; }
      if ( this.jsp2 && this.jsp2.destroy ) { this.jsp2.destroy(); this.jsp2 = null; }
    }
  }
});

Router = Backbone.Router.extend({
  routes: {
    '(/)': 'home',
    'about(/)': 'about',
    'act/:id/:slug(/)': 'acts'
  },
  home: function() {
    ga('send', 'pageview', '/');
    app.render('home');
    for ( act in app.acts ) { app.acts[act].deactivate(); }
  },
  about: function() {
    ga('send', 'pageview', '/about');
    app.render('about');
    for ( act in app.acts ) { app.acts[act].deactivate(); }
  },
  acts: function(id, slug) {
    // redirect to home
    if ( !id || !slug ) {
      app.router.navigate('/', true);
      return;
    }

    ga('send', 'pageview', '/act/' + id + '/' + slug);

    app.render('acts act-' + id);

    if ( app.acts['act-'+id] ) {
      for ( act in app.acts ) {
        if ( act === 'act-'+id ) {
          app.acts[act].activate();
        } else {
          app.acts[act].deactivate();
        }
      }
    }
  }
});

$(document).ready(function() {

  app = new App();
  Backbone.history.start({ pushState: true, root: '/' });

});